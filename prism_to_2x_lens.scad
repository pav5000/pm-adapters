// Общая высота крепления линзы
h = 5;
// Внутренний диаметр
d = 20;
// Конусность внутреннего диаметра (+-)
delta_d = 0.5;
// Ширина выступа упора линзы
stopper_w = 1;
// Толщина стенок упора линзы
wall_w = 1.5;
// Толщина крышки призмы
prism_cap_h = 1;
// Длина крышки призмы
prism_cap_l = 32;
// Ширина крышки призмы
prism_cap_w = 29;
// Длина призмы
prism_l = 26;
// Ширина призмы
prism_w = 19.5;
// Добавочный край крышки
prism_cap_offset = 1;

$fa = 1;
$fs = 0.1;

lens_offset = [1+prism_w/2,prism_cap_l/2,prism_cap_h];
union() {
    difference () {
        prism_cap(
            w=prism_cap_w,
            l=prism_cap_l,
            h=prism_cap_h,
            offs=prism_cap_offset
        );
        translate([lens_offset[0],lens_offset[1],-1])
            cylinder(h=prism_cap_h+3, r=d/2-stopper_w);
    }
    translate(lens_offset) lens_holder();
}

translate ([0,40,0]) {
    difference() {
        prism_cap(
            w=prism_cap_w,
            l=prism_cap_l,
            h=0.6,
            offs=prism_cap_offset
        );
        base_w = 1;
        translate ([base_w,base_w,-1])
            prism_cap(
                w=prism_cap_w-base_w*2,
                l=prism_cap_l-base_w*2,
                h=prism_cap_h+2,
                offs=prism_cap_offset+2
            );
    }
}

module ring(h, r1, r2) {
    difference() {
        cylinder(h=h, r=r2);
        translate([0, 0, -1]) cylinder(h=h+2, r=r1);
    }
}

module prism_cap(w, l, h, offs) {
    // w = prism_cap_w;
    // l  = prism_cap_l;
    r  = 65;
    difference() {
        translate([-offs,0,0])
            cube([w+offs,l,h], center=false);
        translate([w-r,l/2,-1]) ring(h=h+2, r1=r, r2=r+l);
    }
}

module lens_holder() {
    difference() {
        cylinder(h=h, r=d/2+wall_w);
        translate([0, 0, -1]) cylinder(h=h+2, r1=d/2-delta_d/2, r2=d/2+delta_d/2);
    }
}
